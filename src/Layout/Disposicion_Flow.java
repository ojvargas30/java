package Layout;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Disposicion_Flow {

    public static void main(String[] args) {

        Marco_Layouts marcoLay=new Marco_Layouts();
        
    }
}

class Marco_Layouts extends JFrame {

    public Marco_Layouts() {

        setTitle("Layouts predefinidos");

        setBounds(200, 200, 300, 200);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setVisible(true);

        Panel_Layouts milami = new Panel_Layouts();
        
        add(milami);
    }
}



class Panel_Layouts extends JPanel {

    public Panel_Layouts() {
        
        
        //---------------------------------------------------
        
        //FLOWLAYOUT
        
        //PRIMER PASO : CREAR INSTANCIA DE ALGUN LAYOUT
        //FlowLayout disposicion=new FlowLayout(FlowLayout.TRAILING);
             
        //SEGUNDO PASO : UTILIZAR METODO PARA PONER la instancia EN EL MARCO
        //milami.setLayout(disposicion);
        
        //VARIANTE DE TODOS LOS PASOS Y POSICIONAMIENTO EN X y Y
        setLayout(new FlowLayout(FlowLayout.CENTER,20,100));
        
        //---------------------------------------------------

        add(new JButton("Amarillo"));
        add(new JButton("Azul"));
        add(new JButton("Rojo"));

    }

}
