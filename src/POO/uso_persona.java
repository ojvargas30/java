

package POO;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;


public class uso_persona {
    public static void main(String[] args){
    
    
        persona[] perso_array=new persona[2];
        
        perso_array[0]=new Empleado2("Javier" , 1000000, 2020,02,19);
        perso_array[1]=new Alumno("Oscar", "Tec_ADSI");
        
        for(persona e:perso_array){
        
            System.out.println(e.dame_nombre() + ", " + e.dame_descripcion());
        
        }
        
    
    }}
    
       
abstract class persona{

public persona(String nom){

nombre=nom;

}

public String dame_nombre(){

return nombre;


}

//METODO A B S T R A C T O ---------------
public abstract String dame_descripcion();





private String nombre;

}   


//----------------------------------------------------


class Empleado2 extends persona{

      //----CONSTRUCTOR------
    //CONSTRUCTOR CON PARAMETROS-----------
public Empleado2(String nom, double sue, int agno, int mes, int dia){
    
    super(nom);    //Llamar al constructor de la clase padre
    
    sueldo=sue;
    
    GregorianCalendar calendario=new GregorianCalendar(agno,mes,dia);
    
    altaContrato=calendario.getTime();
  
    
    
}

public String dame_descripcion(){

return "Este empleado tiene un sueldo = " + sueldo; 

}



public double dame_sueldo(){   //getter   //POSIBLE METODO FINAL AQUI

return sueldo;

}

public Date dame_fecha_contrato(){  //getter

return altaContrato;

}


public void sube_sueldo(double porcentaje){  //Setter

double aumento= sueldo*porcentaje/100;

sueldo+=aumento;

}





private double sueldo;
private Date altaContrato;

}



class Alumno extends persona{

public Alumno(String nom, String carrera){

    super(nom);
    
    this.carrera=carrera;

}

public String dame_descripcion(){

return "Este alumno esta estudiando la carrera: " + carrera;

}



private String carrera;

}
