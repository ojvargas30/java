

package POO;

//--------------------------------------
//------Creacion de la clase------------
//--------------------------------------

public class Celular {
    
    private String numero;
    private String num_fre_1;
    private String num_fre_2;
    private String num_fre_3;
    private long numeros_frecuentes;
    private float saldo;
    private String mensaje;
    
    
//--------------------------------------
//------Creacion del constructor--------
//--------------------------------------
    public Celular(float saldo_inicial){
    
    this.num_fre_1="3208557457";
    this.num_fre_2="3118330760";
    this.num_fre_3="3214668494";
    saldo=saldo_inicial;
    
    }
    
//--------------------------------------
//-------------METODOS------------------
//--------------------------------------
    
    
    public void set_llamar_Ingrese_numero(String numero){
        
        this.numero=numero;
    
        
       if((numero == null ? num_fre_1 == null : numero.equals(num_fre_1)) || (numero == null ? num_fre_2 == null : numero.equals(num_fre_2)) || (numero == null ? num_fre_3 == null : numero.equals(num_fre_3))
               && saldo>=2.50f){
       
           System.out.println("Llamando al numero: " + numero +
                   "  desde el celular " + "  ring.....");
           System.out.println("Llamada iniciada...... ");
           System.out.println("..............");
           System.out.println("Llamada finalizada");
           System.out.println("--------------");

           
           saldo-=2.50f;
       
       }else if((numero == null ? num_fre_1 != null : !numero.equals(num_fre_1)) || (numero == null ? num_fre_2 != null : !numero.equals(num_fre_2)) || (numero == null ? num_fre_3 != null : !numero.equals(num_fre_3))
               && this.saldo>=5.50f){
       
           System.out.println("Llamando a un numero no frecuente: "
                   + numero + "  ring.....");
           System.out.println("Llamada iniciada........");
           System.out.println("..............");
           System.out.println("Llamada finalizada");
           System.out.println("--------------");

       
           saldo-=5.50f;

       }else{
       
           System.out.println("Marcacion errada o no tiene saldo");
           System.out.println("--------------");

       }
  
        
    }
    
    
    
    
    public void set_mensajear_Ingrese_mensaje(String mensaje){
        
        this.mensaje=mensaje;
    
       if(saldo>=2.00f){
       
           System.out.println("Enviando el mensaje: " + mensaje + "  ring.....");
           System.out.println("Mensaje en espera...... ");
           System.out.println("..............");
           System.out.println(" Mensaje Enviado ");
           System.out.println("--------------");
           System.out.println(" ");
           
           saldo-=2.00f;
           
           
       }else{
       
           System.out.println("No tiene saldo para envi"
                   + "ar un mensaje. Recargue");
           System.out.println("--------------");
           System.out.println(" ");


       }}
       
    public float get_Averiguar_Saldo(){
        
               System.out.println("--------------");
               System.out.println("Su saldo es: $" + saldo);
               System.out.println("--------------");
               System.out.println(" ");
                          
    return saldo;

    }
    
    
    public void set_Hacer_Recarga(float recarga){
    
    saldo=saldo+=recarga;
    
    
                   System.out.println("--------------");

        System.out.println("------- Recarga de $" + recarga +  " efectuada con exito! ----");
          
        
                    System.out.println("--------------");

    }
    


}

class uso_celular{

public static void main(String[] args){

    Celular huawei=new Celular(10.12f);
    Celular samsung=new Celular(2.50f);

    //LLAMAR
    System.out.println("Celular Huawei: ");
    huawei.set_llamar_Ingrese_numero("3208557457");
    
    System.out.println("Celular Samsung: ");
    samsung.set_llamar_Ingrese_numero("3208557457");

    //ENVIAR MENSAJE    
    System.out.println("Celular Huawei: ");
    huawei.set_mensajear_Ingrese_mensaje("Hola como estas, Dios te bendiga. ");
    
    System.out.println("Celular Samsung: ");
    samsung.set_mensajear_Ingrese_mensaje("Hacer un celular en java me llevo bastante.");
   

    
     //CARGAR SALDO
     System.out.println("Celular Huawei: ");
     huawei.set_Hacer_Recarga(10.20f);
     
    
     System.out.println("Celular Samsung: ");
     samsung.set_Hacer_Recarga(50.60f);
     
     
     //CONOCER SALDO
     System.out.println("Celular Huawei: ");
     huawei.get_Averiguar_Saldo();
    
     System.out.println("Celular Samsung: ");
     samsung.get_Averiguar_Saldo();


}
}