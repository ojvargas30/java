

package POO;

import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;


public class uso_empleado {
    
   public static void main(String[] args){
       
   //CODIGO ALTERNATIVO
       //Creamos un array
       
       Scanner ingresar=new Scanner(System.in);
       
       System.out.println("Ingrese la cantidad de empleados: ");
       
       byte C=ingresar.nextByte();
       
       //---------------------------------------
       Jefatura jefe_RRHH=new Jefatura("Oscar",959,2020,03,18);
       
       jefe_RRHH.establece_incentivo(65.400);
       //---------------------------------------

       
       Empleado[] emple_array=new Empleado[C];
       
       //No se puede meter varios diferentes datos en un array en JAVA
       
       //No he encontrado el metodo paa rellenar un array automaticamente
       
       emple_array[0]=new Empleado("Oscar Vargas",92000, 2020,03,14);
       emple_array[1]=new Empleado("Rodolfo",50000, 2020,03,15);
       emple_array[2]=new Empleado("Luna Vargas",80000, 2021,03,25);
       emple_array[3]=new Empleado("Claudelby");
       emple_array[4]=new Empleado(9999);
       
       emple_array[5]=jefe_RRHH; //----POLIFORMISMO---principio de sustitucion
                                 //---- en accion!------
                                 //Es posible porque jefe es un empleado
       
       emple_array[6]=new Jefatura("mARCELA", 100,2020,02,15);
       
       Jefatura jefe_logistico=(Jefatura)emple_array[6];
       
       jefe_logistico.establece_incentivo(80000);
       
       Empleado director_comercial=new Jefatura("Oscar Vargas",92000, 2020,03,14);
       
       //INTERFAZ lll
      
       System.out.println(jefe_logistico.dame_nombre() + " Su pago es: "
               + jefe_logistico.establece_bonus(2000) );
       
       System.out.println(emple_array[1].dame_nombre() + " Su pago es: "
               + emple_array[1].establece_bonus(1000) );
       
       
       //INTERFAZ_ll
       
       System.out.println(jefe_logistico.tomar_desiciones("Dar mas tiempo de descanso y capacitaciones "
               + "especializadas en cada area."));
       
       //INTERFAZ
       
       System.out.println(jefe_logistico.tomar_desiciones("Capacitacion los fines de semana"));
       
       //---NO SE PUEDE INSTANCIAR PERO-------SI--------
       
       Comparable ejemplo=new Empleado("Luna Vargas",80000, 2021,03,25);
       
   
       //USO DE INSTANCE OF
       /*
       
       if(director_comercial instanceof Empleado){
       
           System.out.println("Es de tipo Jefatura");
       
       }
       
       if(ejemplo instanceof Comparable){
       
           System.out.println("Implementa la interfaz comparable");
           
       
       }
       
       
       */
       
       
       
       //ANTIEJEMPLO
       
       //----Jefatura jefe_ventas=(Jefatura)emple_array[1];----
       
       //----------Bucle FOR MEJORADO-----------
       
       for(Empleado r:emple_array){
       
           r.sube_sueldo(10);
       
       }
       
       Arrays.sort(emple_array);
       
       for(Empleado m: emple_array){
       
          System.out.println("Nombres: " + m.dame_nombre()
       + "-- Sueldo: " + m.dame_sueldo() + "-- Fecha-Contratacion: "
        + m.dame_fecha_contrato()); 
       
       }
       

       
       
   }

}

class Empleado implements Comparable, trabajadores{

      //----CONSTRUCTOR------
    //CONSTRUCTOR CON PARAMETROS-----------
public Empleado(String nom, double sue, int agno, int mes, int dia){

    nombre=nom;
    
    sueldo=sue;
    
    GregorianCalendar calendario=new GregorianCalendar(agno,mes,dia);
    
    altaContrato=calendario.getTime();
  
    
    
}

//---------------------------------------
//----------M E T O D O S-----------------------------
//---------------------------------------
public double establece_bonus(double gratificacion){
    
       double prima=2000;

    return trabajadores.bonus_base+prima+gratificacion;

}


public Empleado(String nom){

this(nom,65699999,2020,02,19);


}

public Empleado(double sue){


this("Anonimo(sin especificar)",sue,2020,02,19);

}








/*
public Empleado(String nom){


    nombre=nom;


}
*/

    //----------CREAR VARIABLES AL FINAL----------NO IMPORTA EL LUGAR EN POO



public String dame_nombre(){     //getter

return nombre;

}


public double dame_sueldo(){   //getter   //POSIBLE METODO FINAL AQUI

return sueldo;

}

public Date dame_fecha_contrato(){  //getter

return altaContrato;

}


public void sube_sueldo(double porcentaje){  //Setter

double aumento= sueldo*porcentaje/100;

sueldo+=aumento;

}

public int compareTo(Object miObjeto){

Empleado otro_empleado=(Empleado)miObjeto;

if(this.sueldo<otro_empleado.sueldo){

    return -1;

}

if(this.sueldo>otro_empleado.sueldo){

   return 1;

}

return 0;

}



private String nombre;
private double sueldo;
private Date altaContrato;

}


//---------------------------------------
//---------------------------------------
//---------------------------------------
//---------------------------------------final en la clase para evitar qeu se herede
class Jefatura extends Empleado implements Jefes{
    
    
public Jefatura(String nom,double sue, int agno, int mes, int dia){
    
    
    super(nom,sue,agno,mes,dia);



}
//---------------------------------------
//----------M E T O D O S-----------------------------
//---------------------------------------
public double establece_bonus(double gratificacion){
    
       double prima=2000;

    return trabajadores.bonus_base+prima+gratificacion;

}

public String tomar_desiciones(String desicion){

return "Un miembro de la direccion ha tomado una desicion: " + desicion;


}



private double incentivo;


public void establece_incentivo(double incentivo){

this.incentivo=incentivo;


}

//---------------------------------------

public double dame_sueldo(){

double sueldo_jefe=super.dame_sueldo();  //AQUI SE GUARDA LO QUE RESULTA DE LA 
                                   // CLASE PADRE

return sueldo_jefe+incentivo;

}

//---------------------------------------

}
//---------------------------------------
//---------------------------------------
//---------------------------------------



class Director extends Jefatura{


public Director(String nom,double sue, int agno, int mes, int dia){

super(nom,sue,agno,mes,dia);


}






}
  //----------BUCLE FOR
       /*
       for(int r=0; r<emple_array.length; r++){
       
       emple_array[r].sube_sueldo(10);
       
       }
       
       
       for(int m=0; m<emple_array.length; m++){
       
       System.out.println("Nombres: " + emple_array[m].dame_nombre()
       + "-- Sueldo: " + emple_array[m].dame_sueldo() + "-- Fecha-Contratacion: "
        + emple_array[m].dame_fecha_contrato());
       
       }*/
       
       
       
       
       
       
       
       
       //MUCHO CODIGO-----------
   /*
       Empleado empleado_1=new Empleado("Oscar Vargas",9200.000, 2020,03,14);
       
       Empleado empleado_2=new Empleado("Victor",500.000, 2020,03,15);
       
       Empleado empleado_3=new Empleado("Luna Vargas",800.000, 2021,03,25);

       empleado_1.sube_sueldo(9);
       
       System.out.println("Nombre empleado: " + empleado_1.dame_nombre()
       + "- Sueldo: " + empleado_1.dame_sueldo() + "- Fecha de alta del contrato: "
               + empleado_1.dame_fecha_contrato());
       
       empleado_2.sube_sueldo(9);
       
       System.out.println("Nombre empleado: " + empleado_2.dame_nombre()
       + "- Sueldo: " + empleado_2.dame_sueldo() + "- Fecha de alta del contrato: "
               + empleado_2.dame_fecha_contrato());
       
       empleado_3.sube_sueldo(9);
       
       System.out.println("Nombre empleado: " + empleado_3.dame_nombre()
       + "- Sueldo: " + empleado_3.dame_sueldo() + "- Fecha de alta del contrato: "
               + empleado_3.dame_fecha_contrato());
       
       */