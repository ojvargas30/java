

package POO;

public class Carro {
    //POO VIDEO1
            
            private final int ruedas;
            private final int largo;
            private final int ancho;
            private final int motor;
            private final int peso_plataforma;
            private String color;
            private int peso_total;
            private boolean aire,sillas_cuero;
            
            //----CONSTRUCTOR------
  public Carro(){            //Darle valor inicial a las variables
  
  ruedas=4;
  largo=2000;
  ancho=300;
  motor=1600;
  peso_plataforma=500;
  
}

//---------------Creacion de un metodo------:D

public String dime_datos_generales(){


return "La plataforma del coche tiene: " + ruedas + " ruedas " +
        " mide : " + largo/1000 + " metros con un ancho de: "
      + ancho + "cm. Y un peso en la plataforma de: " + peso_plataforma + " kg " ;   //GETTER

}


public void asigna_color(String elige_color){

//color="Dorado";      //SETTER

    color=elige_color;
    
    
    
}


public String dime_color(){


return "El color del coche es: " + color; //GETTER


}


public void configura_asientos(String asientos_cuero){  //SETTER-----

//this.sillas_cuero=asientos_cuero;   /incompatible types
    
    if(asientos_cuero.equalsIgnoreCase("si")){
    
    this.sillas_cuero=true;
    
    }else{
    
    this.sillas_cuero=false;
    
    }}
    
    

    
public String dime_asientos(){


if(sillas_cuero==true){

return "El carro tiene asientos de cuero";

}else{

return "El carro tiene asientos de serie";

}


}    


public void configura_climatizador(String climatizador){  //SETTER-----

//this.sillas_cuero=asientos_cuero;   /incompatible types
    
    if(climatizador.equalsIgnoreCase("si")){
    
    this.aire=true;
    
    }else{
    
    this.aire=false;
    
    }}


public String dime_aire(){      //GETTER--------------



if(aire==true){

    return "El carro tiene aire";
    

}else{

    return "El carro no tiene aire";

}




}

//--------------------------------------------
//--------SETTER Y GETTER COMBINADOS----------
//--------------------------------------------
public String dime_peso_coche(){

int peso_carroceria=500;

peso_total=peso_plataforma+peso_carroceria;

if(sillas_cuero==true){

peso_total=peso_total+50;

}

if(aire==true){

peso_total=peso_total+19;

}

return "El peso del carro es: " + peso_total;
}


public int dime_precio(){

int precio_total=10000;

if(sillas_cuero==true){

precio_total+=2000;

}
if(aire==true){

precio_total+=1500;

}

return precio_total;


}













}







