

package POO;

import javax.swing.Timer;
import javax.swing.*;
import java.util.*;
import java.awt.Toolkit;
import java.awt.event.*;


public class Clase_Interna_EjTem {
    
         public static void main(String[] args) {
         
         reloj mi_reloj=new reloj(2000, true);
         
         mi_reloj.enMarcha();
         
         JOptionPane.showMessageDialog(null, " Pulsa aceptar para finalizar. ");
         
         System.exit(0);
         
         
         
         
         }

}



class reloj{

//      CADA VEZ QUE VA A A SONAR Y SI QUIERES QUE SUENE O NO
    public reloj(int intervalo, boolean sonido){
    
    this.intervalo=intervalo;
    this.sonido=sonido;
    
    
    }
    
    
    public void enMarcha(){
    
    Damelahora_2 oyente=new Damelahora_2();

    //ActionListener oyente=new Damelahora_2();
    
    //EJECUTA EL TEMPORIZADOR
    
    Timer Mi_temporizador=new Timer(intervalo,oyente);
    
    Mi_temporizador.start();
    
    
    
    }

private int intervalo;
private boolean sonido;


//    CLASE INTERNA QUE ES LA QUE IMPLEMETA LA INTERFAZ


private class Damelahora_2 implements ActionListener{

    
    // - RECORDAR QUE AL IMPLEMENTAR UNA INTERFAZ ALGUNAS PIDEN
    //    QUE IMPLEMENTEMOS TAMBIEN SUS METODOS.
    
    
public void actionPerformed(ActionEvent evento){

Date ahora=new Date();


    System.out.println("Te pongo la hora cada 2 segundos: " + ahora);
    
    
    if (sonido==true) {
        
        Toolkit.getDefaultToolkit().beep();
        
    }



}



}


}
