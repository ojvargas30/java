

package POO;

import javax.swing.Timer;
import javax.swing.*;
import java.util.*;
import java.awt.Toolkit;
import java.awt.event.*;

public class Clase_Interna_Local {
    
    public static void main(String[] args) {
             
      reloj_2 mireloj_2=new reloj_2(); 
             
      mireloj_2.en_Marcha_2(2500, true);
      
      JOptionPane.showMessageDialog(null, "Pulsa aceptar para detener. ");
      
      System.exit(0);
             
             
             }}


class reloj_2{
   
    
    

    // METODO
    
    public void en_Marcha_2(int intervalo, final boolean sonido){
        
        //----------------------------
        //----CLASE INTERNA LOCAL-----
        //----------------------------
        
     class Damelahora_3 implements ActionListener{


// METODO DE LA INTERFAZ - OBLIGATORIO
    
    public void actionPerformed(ActionEvent evento){
    
    Date ahora=new Date();
    
        System.out.println("Te pongo la hora cada 2.5 segundos: " + ahora);
    
    if(sonido){
    
    Toolkit.getDefaultToolkit().beep();
    
    
    }}
}
     
        Damelahora_3 oyente=new Damelahora_3();
        
        Timer mitempo=new Timer(intervalo, oyente);
        
        mitempo.start();
    
        
    
    }



}

