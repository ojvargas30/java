
package POO;


public class Furgoneta extends Carro{
    
    private int plazas_extra;
    private int capacidad_carga;
    
    
    
 public Furgoneta(int plazas_extra, int capacidad_carga){
     
  //-----------------------------------------------------------------------
  super();  //Llama al constructor de la clase cohe y le da un estado inicial-
  //-----------------------------------------------------------------------
  
  this.capacidad_carga=capacidad_carga;   //Diferenciar el parametro de la variable
  
  this.plazas_extra=plazas_extra;


}
 
 
 
 public String dime_datos_furgoneta(){
 
 
 return "La capacidad de carga es: " + capacidad_carga + 
         " y las plazas extra son: " + plazas_extra;
 
 
 }
    
   
}
