/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Eventos;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;

public class Eventos_Raton_l {
    
    public static void main(String[] args) {
        
        marco_raton ratonear = new marco_raton();
        
    }
}

class marco_raton extends JFrame {
    
    public marco_raton() {
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        setBounds(200, 300, 200, 300);
        
        EventoRatata remi = new EventoRatata();
        
        addMouseMotionListener(remi);
        
        setVisible(true);
        
    }
    
}




class EventoRatata implements MouseMotionListener {
    
    public EventoRatata() {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        
        System.out.println(" Estas arrastrando ");
        
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        
        
        System.out.println("Estas moviendo");
        
        
    }
    
    
}

// MOLESTANDO A UN PERSONAJE
/* @Override
    public void mouseEntered(MouseEvent e) {

        // System.out.println("Coordenadas X: " + e.getX() + " Coordenadas Y: " + e.getY());
    }
    
    @Override
    public void mousePressed(MouseEvent e) {

        // System.out.println("Conteo con el method de conteo:" + e.getClickCount());
        if (e.getModifiersEx() == MouseEvent.BUTTON2_DOWN_MASK) {
            
            System.exit(0);
            
        }else if(e.getModifiersEx()== MouseEvent.BUTTON1_DOWN_MASK){
        
          if (e.getClickCount()> 1 && e.getClickCount()<3) {
              
              System.out.println(" Hola en que te puedo ayudar ");
   
          }else if(e.getClickCount()> 2 && e.getClickCount()<4){
          
              System.out.println(" ¿Si? ¿Cual es tu problema porque me tocas? ");

          
          }else if(e.getClickCount()> 3 && e.getClickCount()<5){
          
          
              System.out.println(" Ya esta hombre dejame en paz ");
  
          
          
          }else if(e.getClickCount()> 4 && e.getClickCount()<6){
          
          
              System.out.println(" ¿Quieres pelea pesado? ");
  
          
          
          }
        
        
        }else if(e.getModifiersEx()== MouseEvent.BUTTON3_DOWN_MASK){
        
        
         System.out.println(" ¿Porque presionas clic derecho? Nadie hace eso"
                 + " en pleno 2020 ");
        
        
        }
    }*/



// EVENTO CON INTERFAZ
/*
class EventoRata implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent e) {

        if (e.getClickCount() == 2) {

            System.out.println("Has hecho click apreciado Oscar");

        }

    }

    @Override
    public void mousePressed(MouseEvent e) {

        System.out.println("Acabas de presionar");
    }

    @Override
    public void mouseReleased(MouseEvent e) {

        System.out.println("Acabas de levantar");

    }

    @Override
    public void mouseEntered(MouseEvent e) {

        System.out.println("Acabas de entrar");

    }

    @Override
    public void mouseExited(MouseEvent e) {

        System.out.println("Acabas de salir");

    }

}
 */
