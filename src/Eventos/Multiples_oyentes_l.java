
package Eventos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Multiples_oyentes_l {
   public static void main(String[] args) {
   
       Marco_Multiples_oyentes marco=new Marco_Multiples_oyentes();
       
       
       
}
 
}


class Marco_Multiples_oyentes extends JFrame{

    public Marco_Multiples_oyentes() {
        
        setTitle("Multiples Oyentes");
        
        setBounds(200,200,300,200);
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        setVisible(true);
        
        Lamina_Multiples_oyentes milami=new Lamina_Multiples_oyentes();
        
        add(milami);
        
    }

}




class Lamina_Multiples_oyentes extends JPanel{

    public Lamina_Multiples_oyentes() {
     
        
        JButton btn1=new JButton(" Nuevo ");
        add(btn1);
        
        boton_cerrar=new JButton("Cerrar todo");
        add(boton_cerrar);
        
        
        OyenteNuevo oyente=new OyenteNuevo();
        btn1.addActionListener(oyente);
        
        
        
    }
    
    
    private class OyenteNuevo implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            
            Marco_Emergente marEmer=new Marco_Emergente(boton_cerrar);
            
            
        }
        
        
   
    }



JButton boton_cerrar;


}


class Marco_Emergente extends JFrame{

    public Marco_Emergente(JButton boton_de_principal) {
     
        //PASO 2: Referencia de boton cerrar pasandolo por parametro
        CierraTodo oyenteCerrar=new CierraTodo();
        boton_de_principal.addActionListener(oyenteCerrar);
        
        contador++;
        setTitle("Ventana " + contador);
        setBounds(40*contador,40*contador,250,100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        
        
    }
    
    
    private class CierraTodo implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            //PASO 1: Utilizar metodo dispose que hereda de window que a la vez hereda de Jframe
            dispose();
            
        }
    
    
  
    }
    
    
    
    private static int contador;
    
    
}




