package Eventos;

import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class Multiples_Fuentes_l {

    public static void main(String[] args) {

        Marco_Plantilla mark = new Marco_Plantilla();

    }

}

class Marco_Plantilla extends JFrame {

    public Marco_Plantilla() {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setBounds(240, 230, 500, 300);

        add(new Panel_Plantilla());

        setVisible(true);

    }

}

class Panel_Plantilla extends JPanel {

    public Panel_Plantilla() {

        AccionColor ama = new AccionColor("Amarillo", new ImageIcon("src/Graphics/logo.jpg"), Color.YELLOW);
        AccionColor azul = new AccionColor("Azul", new ImageIcon("src/Graphics/logo.jpg"), Color.BLUE);
        AccionColor rojo = new AccionColor("Rojo", new ImageIcon("src/Graphics/logo.jpg"), Color.RED);

        add(new JButton(ama));
        add(new JButton(azul));
        add(new JButton(rojo));

        /*
        JButton btn_ama = new JButton("Yellow");
        JButton btn_azul= new JButton("Blue");
        JButton btn_rojo = new JButton("Red");

        // AGREGAR BOTONE A LA LAMINA PANEL
        add(btn_ama);
        add(btn_azul);
        add(btn_rojo);*/
        //-------------------------------------------------------------------------
        //-------------------------------------------------------------------------
        //ASIGNANDO ACCIONES AL TECLADO
        //PRIMER PASO
        //InputMap mapaEntrada = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        //PASOS ALTERNOS DEL PRIMER PASO
        InputMap mapaEntrada = getInputMap(JComponent.WHEN_FOCUSED);
        //InputMap mapaEntrada = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        //SEGUNDO PASO
        KeyStroke llaveToque_AMARILLO = KeyStroke.getKeyStroke("ctrl A");
        KeyStroke llaveToque_AZUL = KeyStroke.getKeyStroke("ctrl E");
        KeyStroke llaveToque_ROJO = KeyStroke.getKeyStroke("ctrl R");

        //TERCER PASO
        mapaEntrada.put(llaveToque_AMARILLO, "fondo_AMARILLO");
        mapaEntrada.put(llaveToque_AZUL, "fondo_AZUL");
        mapaEntrada.put(llaveToque_ROJO, "fondo_ROJO");

        //PASO ALTERNO DEL SEGUNDO Y TERCER PASO
        mapaEntrada.put(KeyStroke.getKeyStroke("ctrl Z"), "fondo_AMARILLO");
        
        //CUARTO PASO
        ActionMap mapaAccion = getActionMap();
        mapaAccion.put("fondo_AMARILLO", ama);
        mapaAccion.put("fondo_AZUL", azul);
        mapaAccion.put("fondo_ROJO", rojo);

        //-------------------------------------------------------------------------
        //-------------------------------------------------------------------------
    }

    //  AHORA LO NUEVO DE LAS MULTIPLES FUENTES
// 5 DE LAS 6 POR AbstractAction
    private class AccionColor extends AbstractAction {

        public AccionColor(String name, Icon icon, Color color_boton) {

            putValue(Action.NAME, name);

            putValue(Action.SMALL_ICON, icon);
            putValue(Action.SHORT_DESCRIPTION, "Poner la lamina de color " + name);
            putValue("Color_de_fondo", color_boton);

        }

        @Override
        public void actionPerformed(ActionEvent e) {

            Color c = (Color) getValue("Color_de_fondo");

            setBackground(c);

            System.out.println("Nombre: " + getValue(Action.NAME) + ", Descripcion: "
                    + getValue(Action.SHORT_DESCRIPTION));

        }

    }

}
