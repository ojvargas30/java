/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Eventos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;



/**
 *
 * @author PC-Vargas
 */
public class Eventoll {
    
    public static void main(String[] args) {
        
      Marco_EvBotonColFondo MiMarc=new Marco_EvBotonColFondo();

        
    }
}


class Marco_EvBotonColFondo extends JFrame{
   
      public Marco_EvBotonColFondo(){
      
      
    // OBTENER MEDIDA DE LA PANTALLA
    Toolkit miPantalla=Toolkit.getDefaultToolkit();
    Dimension sizePantalla=miPantalla.getScreenSize();
    int anchoPantalla=sizePantalla.width;
    int alturaPantalla=sizePantalla.height;
    
    // CENTRAR FRAME
    setLocation(anchoPantalla/4,alturaPantalla/4);
   
    // ESTABLECER EL TAMAÑO DEL FRAME
    //setLocation(null); //NO SIRVE
    setSize(anchoPantalla/2,alturaPantalla/2);
    
    // CERRAR FRAME
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    // PONER TITULO
    setTitle("Solumobil | Login");
    
    // ESPECIFICAR IMAGEN ENVES DEL LOGO        
    Image miImagen=miPantalla.getImage("src/graphics/logo.jpg");
    
    // PONER IMAGEN ENVES DEL LOGO
    setIconImage(miImagen);
    
    // INSTANCIAR LAMINA CON FIGURAS
    Lamina_EvBotonColFondo miLaminaE=new Lamina_EvBotonColFondo();
    
    // AÑADIR LAMINA
    add(miLaminaE);
    
    //Color atodo
    miLaminaE.setForeground(Color.RED);
    
    // HACER VISIBLE EL FRAME (DE ULTIMAS)
    setVisible(true);
    
   
      }

}


class Lamina_EvBotonColFondo extends JPanel implements ActionListener{
 
    // como incluir un boton
    JButton bVerde=new JButton("Verde");
    JButton bAzul=new JButton("Azul");
    JButton bRojo=new JButton("Rojo");

       
    
    public Lamina_EvBotonColFondo(){
    
        add(bVerde);
        add(bAzul);
        add(bRojo);
        
        ColorFondo Verde=new ColorFondo(Color.GREEN);
        ColorFondo Azul=new ColorFondo(Color.BLUE);
        ColorFondo Rojo=new ColorFondo(Color.RED);

        
        bVerde.addActionListener(Verde);
        bAzul.addActionListener(Azul);
        bRojo.addActionListener(Rojo);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    
   private class ColorFondo implements ActionListener{

   public ColorFondo(Color c){

        colordefondo=c;

     }
   
   @Override
        public void actionPerformed(ActionEvent e){

        setBackground(colordefondo);

   }

   
   
   private Color colordefondo;

   }
}



