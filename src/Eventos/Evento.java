

package Eventos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.*;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Evento {
    public static void main(String[] args) {
        
      Marco_Evento_Boton MiMarcoEB=new Marco_Evento_Boton();

        
    }
}


class Marco_Evento_Boton extends JFrame{
   
      public Marco_Evento_Boton(){
      
      
    // OBTENER MEDIDA DE LA PANTALLA
    Toolkit miPantalla=Toolkit.getDefaultToolkit();
    Dimension sizePantalla=miPantalla.getScreenSize();
    int anchoPantalla=sizePantalla.width;
    int alturaPantalla=sizePantalla.height;
    
    // CENTRAR FRAME
    setLocation(anchoPantalla/4,alturaPantalla/4);
   
    // ESTABLECER EL TAMAÑO DEL FRAME
    //setLocation(null); //NO SIRVE
    setSize(anchoPantalla/2,alturaPantalla/2);
    
    // CERRAR FRAME
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    // PONER TITULO
    setTitle("Solumobil | Login");
    
    // ESPECIFICAR IMAGEN ENVES DEL LOGO        
    Image miImagen=miPantalla.getImage("src/graphics/logo.jpg");
    
    // PONER IMAGEN ENVES DEL LOGO
    setIconImage(miImagen);
    
    // INSTANCIAR LAMINA CON FIGURAS
    Lamina_Evento_Boton miLaminaE=new Lamina_Evento_Boton();
    
    // AÑADIR LAMINA
    add(miLaminaE);
    
    //Color atodo
    miLaminaE.setForeground(Color.RED);
    
    // HACER VISIBLE EL FRAME (DE ULTIMAS)
    setVisible(true);
    
   
      }

}


class Lamina_Evento_Boton extends JPanel implements ActionListener{
 
    // como incluir un boton
    JButton miboton=new JButton("Verde");
    JButton miboton2=new JButton("Azul");
    JButton miboton3=new JButton("Rojo");

       
    
    public Lamina_Evento_Boton(){
    
        add(miboton);
        add(miboton2);
        add(miboton3);
        
        miboton.addActionListener(this);
        miboton2.addActionListener(this);
        miboton3.addActionListener(this);

    }
        
   // SOBRESCIRBIR METODOS DE LA INTERFAZ
        
    @Override
    public void actionPerformed(ActionEvent e){
        
        Object difBoton=e.getSource();
        
        if (difBoton==miboton) {
            
            setBackground(Color.GREEN);
            
        }else if(difBoton==miboton2){
        
         setBackground(Color.BLUE);
        
        
        }else if(difBoton==miboton3){
        
        setBackground(Color.RED);
        
        }else{
        
            System.out.println("Impossible nothing");
        
        }
        
    }

       
    
}