/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Eventos;

import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author PC-Vargas
 */
public class Evento_Foco_l {

    public static void main(String[] args) {

        marco_foco foca = new marco_foco();

    }

}

class marco_foco extends JFrame {

    public marco_foco() {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setBounds(300, 300, 500, 300);

        add(new lamina_foco());

        setVisible(true);

    }

}

class lamina_foco extends JPanel {

    public lamina_foco() {
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        //INVALIDAR EL LAYOUT
        setLayout(null);

        //CUADROS DE TEXTO 
        cuadro1 = new JTextField();
        cuadro2 = new JTextField();

        cuadro1.setBounds(170, 40, 80, 20);
        cuadro2.setBounds(170, 10, 80, 20);

        add(cuadro1);
        add(cuadro2);

        //INSTANCIA DE LA CLASE DE FOCAS
        Lanzafoco foca = new Lanzafoco();

        cuadro1.addFocusListener(foca);

    }

    JTextField cuadro1;
    JTextField cuadro2;

    // CLASE INTERNA
    private class Lanzafoco implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {

            // System.out.println("Has ganado el foco");
        }

        @Override
        public void focusLost(FocusEvent e) {
            
            
         cuadro1 = (JTextField) e.getSource();
            String correo = cuadro1.getText();

            boolean validacion = false;

            for (int i = 0; i < correo.length(); i++) {

                if (correo.charAt(i) == '@') {

                    validacion = true;

                }

            }

            if (validacion) {
                
                System.out.println("Correcto");
                
            }else{
            
            System.out.println("Debe incluirse una '@'");
                
            }

            //System.out.println("Has perdido el foco");
        }

    }

}
