/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Eventos;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import javax.swing.JFrame;

/**
 *
 * @author PC-Vargas
 */
public class EventoCEV {
public static void main(String[] args) {
        
      Marco_Estado_Ventana MiMarc=new Marco_Estado_Ventana();


        
    }
}


class Marco_Estado_Ventana extends JFrame{
   
      public Marco_Estado_Ventana(){
      
      
    // OBTENER MEDIDA DE LA PANTALLA
    Toolkit miPantalla=Toolkit.getDefaultToolkit();
    Dimension sizePantalla=miPantalla.getScreenSize();
    int anchoPantalla=sizePantalla.width;
    int alturaPantalla=sizePantalla.height;
    
    // CENTRAR FRAME
    setLocation(anchoPantalla/4,alturaPantalla/4);
   
    // ESTABLECER EL TAMAÑO DEL FRAME
    //setLocation(null); //NO SIRVE
    setSize(anchoPantalla/2,alturaPantalla/2);
    
    // CERRAR FRAME
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    
    // PONER TITULO
    setTitle("Solumobil | Login");
    
    // ESPECIFICAR IMAGEN ENVES DEL LOGO        
    Image miImagen=miPantalla.getImage("src/graphics/logo.jpg");
    
    // PONER IMAGEN ENVES DEL LOGO
    setIconImage(miImagen);
    
    
    //INSTANCIA
    Cambia_Estado nuevoEstado=new Cambia_Estado();
    
    addWindowStateListener(nuevoEstado);
    
    // HACER VISIBLE EL FRAME (DE ULTIMAS)
    setVisible(true);
    
   
      }

}


class Cambia_Estado implements WindowStateListener{

    @Override
    public void windowStateChanged(WindowEvent e) {
        
        //System.out.println("La ventana ha cambiado de estado. ");
        
         //System.out.println(e.getNewState());
        
        if (e.getNewState()==Frame.MAXIMIZED_BOTH) {
            
            System.out.println("PANTALLA COMPLETA 'F11'");
            
        }else if(e.getNewState()==Frame.NORMAL){
        
            System.out.println("La pantalla esta normal papu.");
        
        }else if(e.getNewState()==Frame.ICONIFIED){
        
            System.out.println("Pantalla minimizada");
        
        
        }
    }


}