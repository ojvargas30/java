

package Graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.imageio.*;


public class Creando_Imagenes {
    public static void main(String[] args){
        
        Marco_Img MiMarcoF=new Marco_Img();

    }}


class Marco_Img extends JFrame{
   
      public Marco_Img(){
      
      
    // OBTENER MEDIDA DE LA PANTALLA
    Toolkit miPantalla=Toolkit.getDefaultToolkit();
    Dimension sizePantalla=miPantalla.getScreenSize();
    int anchoPantalla=sizePantalla.width;
    int alturaPantalla=sizePantalla.height;
    
    // CENTRAR FRAME
    setLocation(anchoPantalla/4,alturaPantalla/4);
   
    // ESTABLECER EL TAMAÑO DEL FRAME
    //setLocation(null); //NO SIRVE
    setSize(anchoPantalla/2,alturaPantalla/2);
    
    // CERRAR FRAME
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    // PONER TITULO
    setTitle("Solumobil | Login");
    
    // ESPECIFICAR IMAGEN ENVES DEL LOGO        
    Image miImagen=miPantalla.getImage("src/graphics/logo.jpg");
    
    // PONER IMAGEN ENVES DEL LOGO
    setIconImage(miImagen);
    
    // INSTANCIAR LAMINA CON FIGURAS
    Lamina_Img miLamina=new Lamina_Img();
    
    // AÑADIR LAMINA
    add(miLamina);
    
    //Color atodo
    miLamina.setForeground(Color.RED);
    
    // HACER VISIBLE EL FRAME (DE ULTIMAS)
    setVisible(true);
    
      }}




class Lamina_Img extends JPanel{

    public Lamina_Img(){
    
    // CONTROLAR EXCEPCION DE LAS IMAGENES
    
    // File miimagen=new File("src/graphics/logo.jpg");
    
    try{
        
    imagen=ImageIO.read(new File("src/graphics/logo.jpg"));
    
    }catch(IOException e){
    
        System.out.println(" La imagen no se encuentra ");
    
    }
    
    }
    
    
@Override
public void paintComponent(Graphics g){

    
    super.paintComponent(g);
     
    Graphics2D Gra2d=(Graphics2D) g; 
    
    // OBTENER ANCHO Y ALTURA DE NUESTRA IMAGEN SM
    int ancho_img=imagen.getWidth(this);
    int altura_img=imagen.getHeight(this);
    
    
    // DIBUJAR IMAGEN
    Gra2d.drawImage(imagen,0, 0, null);
    
    // COPIANDO IMAGENES
    for (int i = 0; i < altura_img; i++) {
        
        for (int j = 0; j < ancho_img; j++) {
                
            if (j+i>1) {
                
               Gra2d.copyArea(0,0,ancho_img,altura_img,ancho_img*i,altura_img*j);

            }
            

        }
    }
    
    
  
}

private Image imagen;

}