

package Graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Creando_Colores {
     public static void main(String[] args){
      
      Marco_Graficos_Colores_2D MiMarco2D=new Marco_Graficos_Colores_2D();
      
      }

}

class Marco_Graficos_Colores_2D extends JFrame{
   
      public Marco_Graficos_Colores_2D(){
      
      
    // OBTENER MEDIDA DE LA PANTALLA
    Toolkit miPantalla=Toolkit.getDefaultToolkit();
    Dimension sizePantalla=miPantalla.getScreenSize();
    int anchoPantalla=sizePantalla.width;
    int alturaPantalla=sizePantalla.height;
    
    // CENTRAR FRAME
    setLocation(anchoPantalla/4,alturaPantalla/4);
   
    // ESTABLECER EL TAMAÑO DEL FRAME
    //setLocation(null); //NO SIRVE
    setSize(anchoPantalla/2,alturaPantalla/2);
    
    // CERRAR FRAME
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    // PONER TITULO
    setTitle("Solumobil | Login");
    
    // ESPECIFICAR IMAGEN ENVES DEL LOGO        
    Image miImagen=miPantalla.getImage("src/graphics/logo.jpg");
    
    // PONER IMAGEN ENVES DEL LOGO
    setIconImage(miImagen);
    
    // INSTANCIAR LAMINA CON FIGURAS
    Lamina_Figuras_Colores_2D miLamina=new Lamina_Figuras_Colores_2D();
    
    // AÑADIR LAMINA
    add(miLamina);
    
    // ASIGNAR COLOR AL FONDO DE LA LAMINA
    Color ama_os= new Color(200,120,204);
    miLamina.setBackground(ama_os);
    //miLamina.setBackground(SystemColor.window);

    // HACER VISIBLE EL FRAME (DE ULTIMAS)
    setVisible(true);
    
    
    
      
      
      }

}


class Lamina_Figuras_Colores_2D extends JPanel{

public void paintComponent(Graphics g){

    super.paintComponent(g);
    
    //INSTANCIA DE LA CLASE Graphics2D Y REFUNDICION DE
    // g de tipo Graphics a tipo Graphics2D
    Graphics2D miGra2d=(Graphics2D) g;
    
    //IMPLEMENTACION DE LA CLASE RECTANGLE2D DE LA INTERFAZ SHAPE
    Rectangle2D miRectan=new Rectangle2D.Double(140,90,200,150);
    
    Rectangle2D miRectan_2=new Rectangle2D.Double(50,50,100,100);
    
    // OBJETOS DE COLOR
    Color ver_os= new Color(200,155,204);
    Color ama_os= new Color(200,120,204);
    Color ver_na= new Color(100,155,204);

    
    miGra2d.setPaint(ver_os.darker());
    
    miGra2d.fill(miRectan);
    
    /*miGra2d.setPaint(Color.cyan);
    
    miGra2d.fill(miRectan_2);
    
    miGra2d.setPaint(Color.MAGENTA);
    
    miGra2d.draw(miRectan_2);
    */
    
    
    miGra2d.setPaint(Color.MAGENTA);
    
    miGra2d.draw(miRectan);
    
    // DIBUJANDO UNA ELIPSE :D
    
    Ellipse2D miElipse=new Ellipse2D.Double();
    
    miElipse.setFrame(miRectan);
    
    miGra2d.setPaint(new Color(155,211,109).brighter());
    
    miGra2d.fill(miElipse);
    
    miGra2d.setPaint(new Color(0,200,155).darker().darker());

    miGra2d.draw(miElipse);

    
    //miGrafico2d.draw(new Line2D.Double(100,100,300,300));
    
    // OBTENER EL CENTRO DEL RECTANGULO
    
    double centerRecx=miRectan.getCenterX();
    double centerRecy=miRectan.getCenterY();
    double radioRec=100;
    
    //Ellipse2D circulo=new Ellipse2D.Double();
    
    //circulo.setFrameFromCenter(centerRecy, centerRecy, centerRecx+radioRec, centerRecy+radioRec);
    
    //miGrafico2d.draw(circulo);
    
    
    
    
   
    
    

}

}

