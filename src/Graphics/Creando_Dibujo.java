

package Graphics;

import javax.swing.*;
import java.awt.*;

public class Creando_Dibujo {
      public static void main(String[] args){
      
      Marco_Dibujos miMrco=new Marco_Dibujos();
      
      }

}

class Marco_Dibujos extends JFrame{
   
      public Marco_Dibujos(){
      
      
    // OBTENER MEDIDA DE LA PANTALLA
    Toolkit miPantalla=Toolkit.getDefaultToolkit();
    Dimension sizePantalla=miPantalla.getScreenSize();
    int anchoPantalla=sizePantalla.width;
    int alturaPantalla=sizePantalla.height;
    
    // CENTRAR FRAME
    setLocation(anchoPantalla/4,alturaPantalla/4);
   
    // ESTABLECER EL TAMAÑO DEL FRAME
    //setLocation(null); //NO SIRVE
    setSize(anchoPantalla/2,alturaPantalla/2);
    
    // CERRAR FRAME
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    // PONER TITULO
    setTitle("Solumobil | Login");
    
    // ESPECIFICAR IMAGEN ENVES DEL LOGO        
    Image miImagen=miPantalla.getImage("src/graphics/logo.jpg");
    
    // PONER IMAGEN ENVES DEL LOGO
    setIconImage(miImagen);
    
    // INSTANCIAR LAMINA CON FIGURAS
    Lamina_Figuras miLamina=new Lamina_Figuras();
    
    // AÑADIR LAMINA
    add(miLamina);
    
    // HACER VISIBLE EL FRAME (DE ULTIMAS)
    setVisible(true);
    
    
    
      
      
      }

}


class Lamina_Figuras extends JPanel{

public void paintComponent(Graphics g){

    super.paintComponent(g);
    
    //g.drawRect(50, 50, 200,200);
    
    //g.drawLine(100, 100, 200, 200);
    
    //g.fillArc(100, 100, 200, 200, 300, 400);

}

}
