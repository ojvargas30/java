

package Pruebas;

import java.awt.GraphicsEnvironment;
import javax.swing.*;


public class SaberFuentes {
    public static void main(String[] args) {
        
        String font=JOptionPane.showInputDialog(" Buscar Fuente ");
        
        boolean verifica=false;
        
        String [] NfuentesSO=GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        
        
        for (String NF:NfuentesSO) {
            
            if (NF.equalsIgnoreCase(font)) {
                
                verifica=true;
            }
            
        }
        if (verifica) {
            
            System.out.println(" Fuente instalada :D ");
            
        }else{
        
        
            System.out.println(" La fuente no ha sido instalada :/");
        
        
        }
    }
  
}
